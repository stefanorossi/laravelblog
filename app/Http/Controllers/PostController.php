<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //create var and store all pots
        $posts = Post::all();

        //return a view passing all var
        return view('posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $this->validate($request, array(
            'title' => 'required | max:255',
            'body' => 'required'
        ));
        //if validation fails it automatically break and goes back
        //crea atuomaticamente una session flash (tramite middleware)
        //nemmet in un var $errors


        //store into db
        $post = new Post;

        $post->title = $request->title;
        $post->body = $request->body;

        $post->save();

        //creo una var in session per il messagg di conferm (flash vale una request)
        //put per permanente in una sessione
        Session::flash('Success', 'Post added successfully');

        //redirect to another page
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::find($id);

        return view('posts.show')->withPost($post);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //leggo i dati e li mem in una var
        $post = Post::find($id);

        //ritorno la view con i dati
        return view('posts.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $this->validate($request, array(
            'title' => 'required | max:255',
            'body' => 'required'
        ));
        //if validation fails it automatically break and goes back
        //crea atuomaticamente una session flash (tramite middleware)
        //nemmet in un var $errors


        //store into db
        $post = Post::find($id);

        $post->title = $request->input('title');
        $post->body = $request->input('body');

        $post->save();

        //creo una var in session per il messagg di conferm (flash vale una request)
        //put per permanente in una sessione
        Session::flash('Success', 'Post successfully saved');

        //redirect to another page
        return redirect()->route('posts.show');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);

        $post->delete();

        Session::flash('Success', 'Post successfully deleted');

        return redirect()->route('posts.index', $post->id);
    }
}
