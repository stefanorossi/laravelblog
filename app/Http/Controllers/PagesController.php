<?php namespace App\Http\Controllers;

class PagesController extends Controller{

    public function getIndex(){
        return view('pages.welcome');
    }

    public function getAbout() {
//        return 'hello about'; //ritorno una stringa
//        $first ='Stefano';
//        $last ='Rossi';
//        $email ='stefanorossi.ti@gmail.com';
//        $full = $first . ' ' . $last;

        //li passo come array
        $data['first'] ='Stefano';
        $data['last'] ='Rossi';
        $data['email'] ='stefanorossi.ti@gmail.com';
        $data['fullname'] = $data['first'] . ' ' . $data['last'];

//        return view('pages.about')->with("fullname", $full);
        //creo direttamente la variabile fullname (il medoto non esiste ma si puo fare)
//        return view('pages.about')->withFullname($full)->withEmail($email);
        return view('pages.about')->withData($data);



    }

    public function getContact() {
//        return 'hello contact'; //ritorno una stringa
        return view('pages.contact');
    }

    //se voglio gestire una richiesta post
//    public function postContact(){
//
//    }


}