@extends('main')

@section('title', 'Home')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1>Hello, people!</h1>
                <p>This is my blog built with laravel 5.5 and bootstrap 3</p>
                <p><a class="btn btn-primary btn-lg" href="{{route('posts.index')}}" role="button">Posts</a></p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <div class="post">
                <h3 class="post-title"> post title </h3>
                <p class="post-content">
                    Warm watermelon can be made tender by varnishing with plain vinegar.
                </p>
                <a href="#" class="btn btn-primary">Read more</a>
            </div>
            <hr>

            <div class="post">
                <h3 class="post-title"> post title </h3>
                <p class="post-content">
                    Warm watermelon can be made tender by varnishing with plain vinegar.
                </p>
                <a href="#" class="btn btn-primary">Read more</a>
            </div>
            <hr>

            <div class="post">
                <h3 class="post-title"> post title </h3>
                <p class="post-content">
                    Warm watermelon can be made tender by varnishing with plain vinegar.
                </p>
                <a href="#" class="btn btn-primary">Read more</a>
            </div>
            <hr>

            <div class="post">
                <h3 class="post-title"> post title </h3>
                <p class="post-content">
                    Warm watermelon can be made tender by varnishing with plain vinegar.
                </p>
                <a href="#" class="btn btn-primary">Read more</a>
            </div>
            <hr>

        </div>

        <div class="col-md-3 col-md-offset-1">
            sidebar
        </div>
    </div>

</div>

@endsection