@extends('main')

@section('title | Edit post')

@section('content')
    <div class="row">


        {!! Form::model($post, array('route' => array('posts.update', $post->id), 'method'=> 'PUT') ) !!}

        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">

            {{Form::label('title', 'Title:' )}}
            {{Form::text('title', null, ['class' => 'form-control input-lg'] )}}

            {{Form::label('body', 'Body:', ['class' => 'form-spacing-top'] )}}
            {{Form::textarea('body', null, ['class' => 'form-control'] )}}

        </div>

        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="well">
                <div class="row">

                    <dl class="dl-horizontal">
                        <dt>Created at:</dt>
                        <dd>{{$post->created_at}}</dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt>Last update:</dt>
                        <dd>{{$post->updated_at}}</dd>
                    </dl>

                </div>

                <div class="row">

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        {!! Html::linkRoute('posts.show', 'Cancel', array($post->id), array( 'class'=>'btn btn-danger btn-block' ) ) !!}

                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        {{ Form::submit('Save', array('class' => 'btn btn-success btn-block')) }}

                    </div>

                </div>


            </div>
        </div>

        {!! Form::close()!!}

    </div>

@endsection