@extends('main')

@section('title | View post')

@section('content')
    <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">

            <h1>{{ $post->title }}</h1>
            <p class="lead"> {{ $post->body }} </p>

        </div>

        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="well">
                <div class="row">

                    <dl class="dl-horizontal">
                        <dt>Created at:</dt>
                        <dd>{{$post->created_at}}</dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt>Last update:</dt>
                        <dd>{{$post->updated_at}}</dd>
                    </dl>

                </div>

                <div class="row">

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        {!! Html::linkRoute('posts.edit', 'Edit', array($post->id), array( 'class'=>'btn btn-primary btn-block' ) ) !!}

                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        {!! Form::open( [ 'route' => [ 'posts.destroy', $post->id ], 'method'=>'DELETE' ] ) !!}
                        {!! Form::submit( 'Delete', array( 'class'=>'btn btn-primary btn-block' ) ) !!}
                        {!! Form::close() !!}

                    </div>

                </div>


            </div>
        </div>
    </div>

@endsection