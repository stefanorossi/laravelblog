@extends('main')

@section('title | Posts')

@section('stylesheets')
    {!! Html::style('css/parsley.css') !!}
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-lg-offset-2">
            <h1>Create post</h1>
            <hr>

            {!! Form::open(['route' => 'posts.store', 'data-parley-validate' => '']) !!}

            {{ Form::label( 'title', 'Title:' ) }}
            {{ Form::text( 'title', null, array( 'class' => 'form-control', 'required' => '' ) ) }}

            {{ Form::label('body', 'Post body:') }}
            {{ Form::textarea( 'body', null, array('class' => 'form-control', 'required' => '') ) }}

            {{ Form::submit('Create post', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px' )) }}
            {!! Form::close() !!}

        </div>
    </div>

@section('scripts')
    {!! Html::script('js/parsley.min.js') !!}
@endsection

@endsection