@extends('main')

@section('title', 'Posts')

@section('content')

    <div class="row">
        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
            <h1>All posts</h1>
        </div>

        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 ">

            <a href="{{Route('posts.create')}}" class='btn btn-primary btn-block btn-h1-spacing'>New</a>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <table class="table">
                <thead>

                    <th>#</th>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Created at:</th>
                    <th> </th>

                </thead>

                <tbody>

                    @foreach($posts as $post)

                        <tr>
                            <th> {{$post->id}} </th>

                            <td> {{$post->title}} </td>

                            <td> {{\Illuminate\Support\Str::limit($post->body, $limit = 100, $end = '...')}} </td>

                            <td> {{$post->created_at}} </td>

                            <td>
                                {!! Form::open( [ 'route' => [ 'posts.destroy', $post->id ], 'method'=>'DELETE' ] ) !!}
                                <a href="{{route('posts.show', $post->id)}}" class="btn btn-default btn-sm">View</a>
                                <a href="{{route('posts.edit', $post->id)}}" class="btn btn-default btn-sm">Edit</a>

                                {!! Form::submit( 'Delete', array( 'class'=>'btn btn-warning btn-sm' ) ) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>


                    @endforeach

                </tbody>

            </table>

        </div>
    </div>


@stop