<!DOCTYPE html>

<html lang="en">

    <head>

        @include('partials.head')

        @yield('stylesheets')

    </head>


    <body>

        @include('partials.nav')

        <div class="container">

            @include('partials.messages')
            @yield('content')

            <hr>

            @include('partials.footer')

        </div>

        @include('partials.scripts')

        @yield('scripts')
    </body>

</html>